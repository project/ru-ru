
Translation of the Drupal 5.x+ interface to Russian.

  * Discuss this translation (http://drupal.ru/node/3610) on drupal.ru site (in Russian).
  * See also Russian Drupal Installator project (http://drupal.org/project/RussianInstaller).
  * Translation of the Drupal interface to Russian for versions 4.6.x and 4.7.x (http://drupal.org/project/ru).


== in Russian
��ॢ�� ����䥩� Drupal 5.x+ �� ���᪨� ��.

  * ���㦤���� ��ॢ��� �� ᠩ� drupal.ru (http://drupal.ru/node/3610).
  * ���᪨� ���⠫���� ��� Drupal (http://drupal.org/project/RussianInstaller).
  * ��ॢ��� ����䥩� Drupal ��� ���ᨩ 4.6.x � 4.7.x (http://drupal.org/project/ru).

��ॢ�� ᤥ��� ������ (��� � � ��⮬ ���� ��㣨� ��ਠ�⮢ ��ॢ����) �� 蠡����� (*.pot) �⠭���⭮�� ����ਡ�⨢� Drupal 5.0. ��ॢ����� ᮮ�饭�� - 2524/2524 (100%).

�� ��ॢ��� �᭮���� �ନ��� Drupal �ᯮ�짮����� �����-���᪨� ᫮���� �ନ��� Drupal (http://drupal.ru/node/3611).

<b>��� ����㧨�� ��ॢ��?</b>
�室�� �� ᢮� ᠩ� ��� �����. ���� � ࠧ��� Modules ("���㫨") (/admin/build/modules), ����砥� ����� Locale. ������ � ��� ������ ࠧ��� Locale ("��ॢ���") (/admin/settings/locale). ���� �㤠.

��� ���� ��뫪� "Import" (/admin/settings/locale/language/import). ��� �ਢ���� ��� �� ��࠭���, ��� �㦭� 㪠����, ����� 䠩� .po ������஢��� � � ����� �� (㪠��� russian). �������� ��뫪� Import, ����, ���� 䠩� ����㧨��� � ���� ������ ᠩ�. �᫨ �� ����㦠�� ��ॢ��� �⤥���� ���㫥�, � ��� ������ ���� ������� ��� ��� ���㫥�, ����� ���� ��⠭������ � ��襩 ��⥬�.
������ � ��� � ᯨ᪥ ��⠭�������� �몮� (/admin/settings/locale) ������ "���᪨�". ���⠢�� ����⨢ ���� ������ ("����祭�") � �⬥��� ��� ��� default. ���� �����஢��.

<b>���᪨� ���⠫���� Drupal</b>
����� ⠪�� ��ᯮ�짮������ ���᪮���� ���⠫���஬, ����� �������� ��⮬���᪨ ��⠭����� ��������� �����஢���� ᠩ� �� Drupal, ������ �� ���⠫��樨 ᮮ�饭�� �� ���᪮� �몥. 
�஥�� Russian Drupal Installator: ᠩ� �஥�� (http://drupal.org/project/RussianInstaller), ��⠫�� CVS (http://cvs.drupal.org/viewcvs/drupal/contributions/profiles/RussianInstaller/) � ���㦤���� �� ᠩ� drupal.ru (http://drupal.ru/node/4195, �� ���᪮� �몥).

